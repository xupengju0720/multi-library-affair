import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

public class AffairTest {
    public static void main(String[] args) {
        multiAffair();
    }

    private static void multiAffair() {
        //获取A数据库连接
        Connection connectionA = getConnecTionA();
        //获取B数据库连接
        Connection connectionB = getConnecTionB();
        //进行查询验证数据
        System.err.println("事务前的验证");
        connectionConfirm(connectionA, connectionB);

        //执行事务
        Affair(connectionA, connectionB);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.err.println("事务后的验证");
        connectionConfirm(connectionA, connectionB);

        try {
            //关闭资源
            connectionA.close();
            connectionB.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }

    private static void Affair(Connection connectionA, Connection connectionB) {
        String sql = "update student set `value` = 20 where `name` = 'wangguoxi'";
        try {
            System.out.println("开启事务A");
            connectionA.setAutoCommit(false);
            PreparedStatement psA = (PreparedStatement) connectionA.prepareCall(sql);
            System.out.println("DataBase->testA,执行sql语句:" + sql);
            psA.execute();//更新数据库
            try {
                System.out.println("开启事务B");
                connectionB.setAutoCommit(false);
                PreparedStatement psB = (PreparedStatement) connectionB.prepareCall(sql);
                System.out.println("DataBase->testB,执行sql语句:" + sql);
                psB.execute();//更新数据库
                System.out.println("提交事务B");
                connectionB.commit();
            } catch (Exception eb) {
                System.out.println("事务B异常：" + eb.getMessage());
                System.out.println("事务B回滚");
                connectionB.rollback();
                //eb.printStackTrace();
                throw new Exception(eb);
            }
            System.out.println("提交事务A");
            connectionA.commit();
        } catch (Exception e) {
            try {
                System.out.println("事务A异常：" + e.getMessage());
                System.out.println("事务A回滚");
                connectionA.rollback();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            //e.printStackTrace();
        }
    }

    private static void connectionConfirm(Connection connectionA, Connection connectionB) {
        ResultSet rs = null;
        Statement stmt = null;
        try {
            stmt = (Statement) connectionA.createStatement();
            rs = stmt.executeQuery("select * from student");
            System.out.println("DataBase->testA,Table->student");
            while (rs.next()) {
                System.out.println(rs.getInt("id") + "\t" + rs.getString("name") + "\t" + rs.getString("value"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            stmt = (Statement) connectionB.createStatement();
            rs = stmt.executeQuery("select * from student");
            System.out.println("DataBase->testB,Table->student");
            while (rs.next()) {
                System.out.println(rs.getInt("id") + "\t" + rs.getString("name") + "\t" + rs.getString("value"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnecTionA() {
        String usernameA = "root";
        String passwordA = "123456";
        String urlA = "jdbc:mysql://localhost:3306/testA?useSSL=true&useUnicode=true&characterEncoding=UTF-8";
        String driverNameA = "com.mysql.cj.jdbc.Driver";
        return new MySqlDB(usernameA, passwordA, urlA, driverNameA).getConnection();
    }

    private static Connection getConnecTionB() {
        String usernameB = "root";
        String passwordB = "123456";
        String urlB = "jdbc:mysql://localhost:3306/testB?useSSL=true&useUnicode=true&characterEncoding=UTF-8";
        String driverNameB = "com.mysql.cj.jdbc.Driver";
        return new MySqlDB(usernameB, passwordB, urlB, driverNameB).getConnection();
    }
}

